from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.http import HttpResponse

# Local imports
from .models import ToDoItem, EventItem
from .forms import LoginForm, RegisterForm, AddTaskForm, UpdateTaskForm, AddEventForm, UpdateEventForm, ChangePasswordForm, UpdateProfileForm

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.forms.models import model_to_dict
from django.utils import timezone


def index(request):
	# todoitem_list = ToDoItem.objects.all()
	todoitem_list = ToDoItem.objects.filter(user_id = request.user.id)
	eventitem_list = EventItem.objects.filter(user_id = request.user.id)

	context = {
		"todoitem_list" : todoitem_list,
		"eventitem_list" : eventitem_list,
		"user" : request.user
	}
	# context = {'todoitem_list' : todoitem_list}

	return render(request, "todolist/index.html", context)
	# return HttpResponse("Hello from the views.py file")

def todoitem(request, todoitem_id):

	# todoitem = model_to_dict(ToDoItem.objects.get(pk=todoitem_id))
	todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)

	# response = "You are viewing the details of %s"

	# return HttpResponse(response %todoitem_id)
	# return render(request, "todolist/todoitem.html", todoitem)
	return render(request, "todolist/todoitem.html", model_to_dict(todoitem))

def register(request):

	context = {}

	if request.method == "POST":
		form = RegisterForm(request.POST)
		if form.is_valid() == False:
			form = RegisterForm()
		else:
			username = form.cleaned_data['username']
			first_name = form.cleaned_data['first_name']
			last_name = form.cleaned_data['last_name']
			email = form.cleaned_data['email']
			password = form.cleaned_data['password']
			confirm_password = form.cleaned_data['confirm_password']

			if password == confirm_password:
				duplicate = User.objects.filter(username = username)

				if not duplicate:
					User.objects.create(username=username, first_name=first_name, last_name=last_name, email=email, password=make_password(password), is_staff = False,is_active = True)
					return redirect("todolist:index")
				else:
					context = {
						"error" : True,
						"username_taken" : True
					}
			else:
				context = {
					"error" : True,
					"passwords_mismatch" : True
				}

	# users = User.objects.all()
	# is_user_registered = False
	# context = {
	# 	"is_user_registered" : is_user_registered
	# }

	# # check if username already exist
	# for indiv_user in users:
	# 	if indiv_user.username == "johndoe":
	# 		is_user_registered = True
	# 		break;
	# if is_user_registered == False:
	# 	user = User() #empty object
	# 	user.username = "johndoe"
	# 	user.first_name = "John"
	# 	user.last_name = "Doe"
	# 	user.email = "john@mail.com"
	# 	# The 'set_password' is used to ensure that the passord is hashed using Django's authentication framework
	# 	user.set_password("John1234")
	# 	user.is_staff = False
	# 	user.is_active = True
	# 	user.save()

	# 	context = {
	# 		"first_name" : user.first_name,
	# 		"last_name" : user.last_name,
	# 	}

	return render(request, "todolist/register.html", context)

def change_password(request):
	# is_user_authenticated = False

	# user = authenticate(username="johndoe", password="John1234")
	# print(user)
	# if user is not None:
	# 	authenticated_user = User.objects.get(username='johndoe') # this is where we get the username "johndoe"
	# 	authenticated_user.set_password("johndoe1") # this is where we change password
	# 	authenticated_user.save()
	# 	is_user_authenticated = True
	# context = {
	# 	"is_user_authenticated": is_user_authenticated
	# }

	context = {
		"user" : request.user
	}
	if request.method == "POST":
		form = ChangePasswordForm(request.POST)
		if form.is_valid() == False:
			form = ChangePasswordForm()
		else:
			password = form.cleaned_data['password']
			user = authenticate(username=request.user.username, password=password)
			context = {
				"username" : request.user.username,
				"password" : password
			}

			if user is not None:
				new_password = form.cleaned_data['new_password']
				confirm_new_password = form.cleaned_data['confirm_new_password']

				if new_password == confirm_new_password:
					user.set_password(new_password)
					user.save()
					return redirect("todolist:index")
				else:
					context = {
						"error" : True,
						"passwords_mismatch" : True
					}					
			else:
				context = {
					"error" : True,
					"incorrect_password" : True
				}

	return render(request, "todolist/change_password.html", context)

def login_view(request):

	context = {}

	if request.method == "POST":
		# Create a form instance and populate it with data from the request
		form = LoginForm(request.POST)
		# Check if data is valid
		# Runs validation for all the form fields and returns and places the form's data in the cleaned_data attribute
		if form.is_valid() == False:
			# Return a blank login form
			form = LoginForm()
		else:
			# Receives the information form the form
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']
			user = authenticate(username=username, password=password)
			context = {
				"username" : username,
				"password" : password
			}
			if user is not None:
				# Saves the user’s ID in the session using Django's session framework
				login(request, user)
				return redirect("todolist:index")
			else:
				# Provides context with error to conditionally render the error message
				context = {
					"error" : True
				}

	return render(request, "todolist/login.html", context)


	# username = "johndoe"
	# password = "johndoe1"
	# user = authenticate(username=username, password=password)
	# context = {
	# 	"is_user_authenticated" : False
	# }
	# print(user)
	# if user is not None:
	# 	login(request, user)
	# 	return redirect("todolist:index")
	# else:
	# 						#templates/todolist/login.html
	# 	return render(request, "todolist/login.html", context)

def logout_view(request):
	logout(request)
	return redirect("todolist:index")

def add_task(request):

	context = {}
	# empty by default, so no value

	if request.method == "POST":
		form = AddTaskForm(request.POST)
		if form.is_valid() == False:
			form = AddTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			duplicate = ToDoItem.objects.filter(task_name = task_name)

			if not duplicate:
				ToDoItem.objects.create(
					task_name=task_name, 
					description=description, 
					date_created=timezone.now(), 
					user_id = request.user.id
				)
				return redirect("todolist:index")
			else:
				context = {
					"error" : True
				}
	return render(request, "todolist/add_task.html", context)

def update_task(request, todoitem_id):

	# Returns a queryset
	todoitem = ToDoItem.objects.filter(pk=todoitem_id)

	context = {
		"user": request.user,
		"todoitem_id": todoitem_id,
			# Accessing the first element is necessary because the "ToDoItem.objects.filter()" method returns a queryset
		"task_name": todoitem[0].task_name,
		"description": todoitem[0].description,
		"status": todoitem[0].status
	}

	if request.method == 'POST':
		form = UpdateTaskForm(request.POST)
		if form.is_valid() == False:
			form = UpdateTaskForm()
		else:
			task_name = form.cleaned_data['task_name']
			description = form.cleaned_data['description']
			status = form.cleaned_data['status']

			if todoitem:
				todoitem[0].task_name = task_name
				todoitem[0].description = description
				todoitem[0].status = status

				todoitem[0].save()
				return redirect("todolist:index")
			else:
				context = {
					"error": True
				}
	return render(request, "todolist/update_task.html", context)

def delete_task(request, todoitem_id):
	todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
	return redirect("todolist:index")


def eventitem(request, eventitem_id):
	eventitem = get_object_or_404(EventItem, pk=eventitem_id)
	return render(request, "todolist/eventitem.html", model_to_dict(eventitem))

def add_event(request):

	context = {}
	if request.method == "POST":
		form = AddEventForm(request.POST)
		if form.is_valid() == False:
			form = AddEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			event_date=form.cleaned_data['event_date']
			duplicate = EventItem.objects.filter(event_name = event_name)

			if not duplicate:
				EventItem.objects.create(
					event_name=event_name, 
					description=description, 
					date_created=timezone.now(),
					event_date=form.cleaned_data['event_date'],
					user_id = request.user.id
				)
				return redirect("todolist:index")
			else:
				context = {
					"error" : True
				}
	return render(request, "todolist/add_event.html", context)

def update_event(request, eventitem_id):

	eventitem = EventItem.objects.filter(pk=eventitem_id)

	context = {
		"user": request.user,
		"eventitem_id": eventitem_id,
		"event_name": eventitem[0].event_name,
		"event_date": eventitem[0].event_date,
		"description": eventitem[0].description,
		"status": eventitem[0].status
	}

	if request.method == 'POST':
		form = UpdateEventForm(request.POST)
		if form.is_valid() == False:
			print(form.errors)
			form = UpdateEventForm()
		else:
			event_name = form.cleaned_data['event_name']
			description = form.cleaned_data['description']
			# event_date = form.cleaned_data['event_date']
			status = form.cleaned_data['status']

			if eventitem:
				eventitem[0].event_name = event_name
				eventitem[0].description = description
				# eventitem[0].event_date = event_date
				eventitem[0].status = status
				eventitem[0].date_modified=timezone.now()

				eventitem[0].save()
				return redirect("todolist:index")
			else:
				context = {
					"error": True
				}
	return render(request, "todolist/update_event.html", context)

def delete_event(request, eventitem_id):
	eventitem = EventItem.objects.filter(pk=eventitem_id).delete()
	return redirect("todolist:index")

def update_profile(request):
	context = {
		"user" : request.user
	}
	if request.method == "POST":
		form = UpdateProfileForm(request.POST)
		if form.is_valid() == False:
			form = UpdateProfileForm()
		else:
			username = form.cleaned_data['username']
			user_exist = User.objects.filter(username = username)

			if not email_exist:
				email = form.cleaned_data['email']
				email_exist = User.objects.filter(email = email)

				if not email_exist:
					first_name = form.cleaned_data['first_name']
					last_name = form.cleaned_data['last_name']
					user.save()
					return redirect("todolist:index")
				else:
					context = {
						"error" : True,
						"email_taken" : True
					}					
			else:
				context = {
					"error" : True,
					"username_taken" : True
				}

	return render(request, "todolist/update_profile.html", context)

#