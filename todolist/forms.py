from django import forms
# from django.db import models
from django.utils import timezone


class LoginForm(forms.Form):
	username = forms.CharField(label="Username", max_length=20)
	password = forms.CharField(label="Password", max_length=20)

class AddTaskForm(forms.Form):
	task_name = forms.CharField(label="Task Name", max_length=20)
	description = forms.CharField(label="Description", max_length=20)

class RegisterForm(forms.Form):
	username = forms.CharField(label="Username", max_length=20)
	first_name = forms.CharField(label="First Name", max_length=20)
	last_name = forms.CharField(label="Last Name", max_length=20)
	email = forms.CharField(label="Email", max_length=20)
	password = forms.CharField(label="Password", max_length=20)
	confirm_password = forms.CharField(label="Confirm Password", max_length=20)


class UpdateTaskForm(forms.Form):
		# If fields are optional, using the "Model.save()" method will not work
		# Using the option "required=False" will allow the field to be optional when updating a record
		# task_name = forms.CharField(label='Task Name', max_length=50, required=False)
	task_name = forms.CharField(label='Task Name', max_length=50, required=False)
	description = forms.CharField(label='Description', max_length=200, required=False)
	status = forms.CharField(label='Status', max_length=50, required=False)


class AddEventForm(forms.Form):
	event_name = forms.CharField(label="Event Name", max_length=50)
	description = forms.CharField(label="Description", max_length=200)
	event_date = forms.DateTimeField(label="Event Date", input_formats=['%Y-%m-%d'])

class UpdateEventForm(forms.Form):
	event_name = forms.CharField(label='Event Name', max_length=50, required=False)
	description = forms.CharField(label='Description', max_length=200, required=False)

	status = forms.CharField(label='Status', max_length=50, required=False)


class ChangePasswordForm(forms.Form):
	password = forms.CharField(label="Password", max_length=20)
	new_password = forms.CharField(label="New Password", max_length=20)
	confirm_new_password = forms.CharField(label="Confirm New Password", max_length=20)

class UpdateProfileForm(forms.Form):
	username = forms.CharField(label="Username", max_length=20, required=False)
	first_name = forms.CharField(label="First Name", max_length=20, required=False)
	last_name = forms.CharField(label="Last Name", max_length=20, required=False)
	email = forms.CharField(label="Email", max_length=20, required=False)
