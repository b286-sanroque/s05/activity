from django.urls import path

from . import views

app_name= 'todolist'

urlpatterns = [
	# The path() function receives four(4) arguments
	# We'll focus on 2 arguments that are rquired: 'route' and 'view'
		#route
			#view			#label, *optional
	path('', views.index, name='index'),

	# todoitem/1 (or 2, 3, ...) <general endpoint
	# The '<int:todoitem_id>/' allows for creating a dynamic link where the todolist_id is provided
	path('<int:todoitem_id>/', views.todoitem, name = "viewtodoitem"),

	path('register', views.register, name = "register"),
	path('change_password', views.change_password, name = "change_password"),
	path('login', views.login_view, name = "login"),
	path('logout', views.logout_view, name = "logout"),
	path('add_task', views.add_task, name = "add_task"),

	# update_task path
	path('<int:todoitem_id>/edit', views.update_task, name='update_task'),
	# /todolist/<todoitem_id>/delete route
	path('<int:todoitem_id>/delete', views.delete_task, name='delete_task'),
	

	path('add_event', views.add_event, name = "add_event"),
	path('events/<int:eventitem_id>/', views.eventitem, name = "vieweventitem"),
	path('events/<int:eventitem_id>/edit', views.update_event, name='update_event'),
	path('events/<int:eventitem_id>/delete', views.delete_event, name='delete_event'),

	path('update_profile', views.update_profile, name = "update_profile"),
]